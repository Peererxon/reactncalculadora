import { SafeAreaView, StatusBar } from 'react-native';
import React from 'react';
import CalculadoraScreen from './src/screens/CalculadoraScreen';
import { styles } from './src/theme/appTheme';
//TODO: Agregar lista con scroll de resultados antiguos y poder llevarlos a pantalla si se seleccionan
export const App =()=> {
  return (
    <SafeAreaView style={ styles.fondo }>
      <StatusBar 
        backgroundColor='black'
        barStyle='light-content'
      />
      <CalculadoraScreen />
    </SafeAreaView>
  );
}


