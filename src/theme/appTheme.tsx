import { StyleSheet } from "react-native";

export const styles = StyleSheet.create({
  calculadoraContainer: {
    paddingHorizontal: 20,
    flex: 1,
    justifyContent: 'flex-end'
  },
  fondo: {
      flex: 1,
      backgroundColor: 'black',
  },
  resultado: {
      color: 'white',
      fontSize: 60,
      textAlign: 'right',
      marginHorizontal: 10
  },
  resultadoSmart: {
    color: 'rgba(255,255,255, 0.5)',
    fontSize: 30,
    textAlign: 'right'  
  },
  fila: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginBottom: 18,
    paddingHorizontal: 10
  },
   boton: {
     height: 80,
     width: 80,
     backgroundColor: 'gray',
     borderRadius: 50,
     justifyContent: 'center',
     marginHorizontal: 10
   },
   botonTexto: {
     textAlign: 'center',
     padding: 10,
     fontSize: 30,
     color: 'white',
     fontWeight: 'bold'
   }
});
