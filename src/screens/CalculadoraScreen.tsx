import { Text, View } from 'react-native';
import React from 'react';
import { styles } from '../theme/appTheme';
import BotonCalc from '../components/BotonCalc';
import { useCalculadora } from '../hooks/Usecalculadora';

const CalculadoraScreen = () => {
  const { 
    numero,
    numeroAnterior,
    armarNumero,
    positiveNegative,
    deleteNumber,
    limpiar,
    botonSumar,
    botonRestar,
    botonMultiplicar,
    botonDividir,
    calcular
  }  = useCalculadora()
  return (
    <View style={ styles.calculadoraContainer }>
      {
        numeroAnterior !== '0' && (
          <Text style={ styles.resultadoSmart }>
            { numeroAnterior }
          </Text>
        )
      }
      <Text 
        style={ styles.resultado }
        numberOfLines={ 1 }
        adjustsFontSizeToFit
      >{ numero }</Text>

      {/* Fila de botones */}
      <View style={ styles.fila }>
        <BotonCalc number='C'   colorButtom='#9B9B9B' action={ limpiar }/>
        <BotonCalc number='+/-' colorButtom='#9B9B9B' action={ positiveNegative }/>
        <BotonCalc number='del' colorButtom='#9B9B9B' action={ deleteNumber }/>
        <BotonCalc number='/'   colorButtom='#FF9427' action={ botonDividir }/>
      </View>

      {/* Fila de botones */}
      <View style={ styles.fila }>
        <BotonCalc number='7' action={ armarNumero } />
        <BotonCalc number='8' action={ armarNumero }/>
        <BotonCalc number='9' action={ armarNumero }/>
        <BotonCalc number='X'   colorButtom='#FF9427' action={ botonMultiplicar }/>
      </View>
      
      {/* Fila de botones */}
      <View style={ styles.fila }>
        <BotonCalc number='4' action={ armarNumero } />
        <BotonCalc number='5' action={ armarNumero }/>
        <BotonCalc number='6' action={ armarNumero }/>
        <BotonCalc number='-' colorButtom='#FF9427' action={ botonRestar } />
      </View>

      {/* Fila de botones */}
      <View style={ styles.fila }>
        <BotonCalc number='1' action={ armarNumero }  />
        <BotonCalc number='2' action={ armarNumero }/>
        <BotonCalc number='3' action={ armarNumero }/>
        <BotonCalc number='+' colorButtom='#FF9427' action={ botonSumar } />
      </View>

      {/* Fila de botones */}
      <View style={ styles.fila }>
        <BotonCalc number='0' ancho action={ armarNumero }/>
        <BotonCalc number='.' action={ armarNumero }/>
        <BotonCalc number='='  colorButtom='#FF9427' action={ calcular }/>
      </View>
    </View>
  );
};

export default CalculadoraScreen;
