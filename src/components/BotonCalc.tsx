import { Text, TouchableOpacity, View } from 'react-native';
import React from 'react';
import { styles } from '../theme/appTheme';

interface Props {
    number: string,
    colorButtom? :string,
    ancho? : boolean,
    action: ( numeroTexto:string )=> void;
}

export default function BotonCalc( { number, colorButtom = '#2D2D2D', ancho, action }: Props ) {
  return (
    <TouchableOpacity 
      activeOpacity={.4}
      onPress={ ()=> action( number ) }
    >
      <View style={
        { ...styles.boton,backgroundColor: colorButtom ,
          width: ( ancho ) ? 180 : 80
        }} 
      >
          <Text style={{
            ...styles.botonTexto,
            color: ( colorButtom === '#9B9B9B' ) ? 'black' : 'white'
          }}
          > { number } </Text>
      </View>

    </TouchableOpacity>
  );
}
